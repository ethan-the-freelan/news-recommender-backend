# Load the rails application
require File.expand_path('../application', __FILE__)
require 'rjb'

# Initialize the rails application
NewsRecommenderBackend::Application.initialize!

# Add classpath for rjb
classpath = [
  "#{Rails.root}/lib/nekohtml-1.9.13.jar",
  "#{Rails.root}/lib/xerces-2.9.1.jar",
  "#{Rails.root}/lib/boilerpipe-1.2.0.jar",
  "#{Rails.root}/lib/VnTextClassification.jar"
].join(File::PATH_SEPARATOR)
Rjb::load(classpath, ['-classpath -Xms128M -Xmx512M'])

BoilerpipeExtractor = Rjb::import('de.l3s.boilerpipe.BoilerpipeExtractor')
CommonExtractors = Rjb::import('de.l3s.boilerpipe.extractors.CommonExtractors')
HTMLHighlighter = Rjb::import('de.l3s.boilerpipe.sax.HTMLHighlighter')
URL = Rjb::import('java.net.URL')
StrUtil = Rjb::import('lib.string.StrUtil')
Classification = Rjb::import('mlearning.maxent.Classification')