NewsRecommenderBackend::Application.routes.draw do
  get "users/browse_article"

  scope "/api" do
    resources :sites, format: false, only: %w[index] do
      collection do
        get :rules
        get :all_info
        get :all_sites
        get :all_categories
      end
    end

    resources :articles, format: false, only: %w[all] do
      collection do
        get :all
        get :by_site
        get :by_category
      end
    end

    resources :classifiers, only: %w[index] do
      collection do
        get :classify
        get :apply
        get :delete_article
      end
    end

    resources :users, only: %w[browse_article] do
      collection do
        get :browsed_articles
        get :recommended_articles
        post :browse_article
      end
    end
  end
end
