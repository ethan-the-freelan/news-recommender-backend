require 'builder'

class Article
  include Mongoid::Document
  include Mongoid::CachedJson
  include Article::Json

  field :url
  field :title
  field :image
  field :summary
  field :posted_time, type: DateTime, default: -> { Time.now }
  field :extracted_content

  field :weight       # weight of article for its category

  belongs_to :site
  belongs_to :category
  has_and_belongs_to_many :related_articles, class_name: "Article", inverse_of: :related_articles
  has_and_belongs_to_many :browsing_users, class_name: "User", inverse_of: :browsed_articles

  validates_presence_of :url, :title
  validates_uniqueness_of :url

  attr_accessible :url, :title, :image, :summary, :posted_time, :extracted_content

  json_fields \
    _id: { },
    url: { },
    title: { },
    image: { },
    posted_time: { definition: :posted_time_formatted },
    summary: { },
    site: { definition: :containing_site },
    category: { definition: :belonging_category }

  def posted_time_formatted
    self.posted_time.rfc822
  end

  def containing_site
    self.site.title
  end

  def belonging_category
    self.category.name
  end

  def to_xml(options = {})
    options[:indent] ||= 2
    xml = options[:builder] ||= ::Builder::XmlMarkup.new(:indent => options[:indent])
    xml.instruct! unless options[:skip_instruct]
    xml.article do
      xml.tag!(:url, url)
      xml.tag!(:title, title)
      xml.tag!(:time, posted_time_rfc)
      xml.tag!(:summary, summary) if summary
    end
  end

  private :posted_time_formatted
end