# encoding: utf-8

module ArticleClassifier::FileUtil
  extend ActiveSupport::Concern

  CATEGORIES_MAP = {
    "CongNghe" => "Cong_nghe",
    "OTo-XeMay" => "Cong_nghe",
    "Dep" => "Thoi_trang_Lam_dep",
    "TinhYeu-HonNhan-GiaDinh" => "Doi_song",
    "SucKhoe" => "Doi_song",
    "AmThuc" => "Doi_song",
    "NhaDep" => "Doi_song",
    "GioiTinh" => "Doi_song",
    "TheThao" => "The_thao",
    "ChinhSu" => "Chinh_tri",
    "TheGioi" => "Chinh_tri",
    "ViecLam" => "Xa_hoi",
    "MoiTruong-ThamHoa" => "Xa_hoi",
    "GiaoDuc" => "Xa_hoi",
    "XayDung" => "Xa_hoi",
    "PhapLuat" => "Phap_luat",
    "GiaoThong" => "Phap_luat",
    "TaiChinh" => "Kinh_te_Tai_chinh",
    "BatDongSan" => "Kinh_te_Tai_chinh",
    "KinhDoanh" => "Kinh_te_Tai_chinh",
    "DuLich" => "Van_hoa_Giai_tri",
    "LeHoi-QuaTang" => "Van_hoa_Giai_tri",
    "Game" => "Van_hoa_Giai_tri",
    "AmNhac-DienAnh-TruyenHinh" => "Van_hoa_Giai_tri"
  }

  def write_test_file(sample_amount = 250)
    test_file = File.open("#{Rails.root}/lib/VnTextClassification/models/test.txt", "a")

    Article.all.sample(sample_amount).each do |sample|
      classified = classify_article sample
      unless classified.nil?
        print "Classify article \"#{sample.title}\" (#{sample.url}) with category \"#{classified[:category]}\"? "
        category = gets.chomp
        if category.blank?
          test_file.write classified[:tokenized_content] + " " + classified[:category] + "\n"
        else
          test_file.write classified[:tokenized_content] + " " + category + "\n"
        end
      end
    end

    test_file.close
  end

  def write_train_file(sample_amount = 1000)
    train_file = File.open("#{Rails.root}/lib/VnTextClassification/models/train.txt", "w")
    train_file.flush

    Article.all.sample(sample_amount).each do |sample|
      classified = classify_article sample
      unless classified.nil?
        print "Classify article \"#{sample.title}\" (#{sample.url}) with category \"#{classified[:category]}\"? "
        category = gets.chomp
        if category.blank?
          train_file.write classified[:tokenized_content] + " " + classified[:category] + "\n"
        else
          train_file.write classified[:tokenized_content] + " " + category + "\n"
        end
      end
    end

    train_file.close
  end

  def write_sample_data_file(name, amount)
      category_articles = {
        "Chinh_tri" => [],
        "Kinh_te_Tai_chinh" => [],
        "Doi_song" => [],
        "The_thao" => [],
        "Cong_nghe" => [],
        "Xa_hoi" => [],
        "Thoi_trang_Lam_dep" => [],
        "Van_hoa_Giai_tri" => [],
        "Phap_luat" => []
      }

      (0..19).each do |i|
        puts "Processing articles in range: #{i*1000} - #{(i+1)*1000}"

        path = "#{Rails.root}/app/assets/sample_data/#{i*1000}-#{(i+1)*1000}.txt.category"
        xml_str = "<SampleData>#{IO.read(path)}</SampleData>".gsub! /[\n\r\t]+/, ""
        
        xml = Nokogiri.XML(xml_str)
        
        xml.xpath("//Document").each do |doc|
          children = doc.children
          category = children.last.text
          children.pop
          
          if category_articles[category].count < amount
            str = children.map {|c| c.text}.join(" ")
            str.gsub! /[^\p{L}_]/, " "
            str.gsub! /(^\s+|\s+$)/, ""
            str.gsub! /\s+/, " "
            category_articles[category] << tokenize_content(str) + " " + category
          end
        end
      end

      content = category_articles.values.flatten
      puts "Writing #{content.count} articles - stats: #{category_articles.values.map {|a| a.count}}"
      test_file = File.open("#{Rails.root}/lib/VnTextClassification/models/#{name}.txt", "w")
      content.each do |article|
        test_file.write article + "\n"
      end
      test_file.close
    end

  module ClassMethods
    def exam_sample_data
      articles_count = 0
      categories_count = {
        "Chinh_tri" => 0,
        "Kinh_te_Tai_chinh" => 0,
        "Doi_song" => 0,
        "The_thao" => 0,
        "Cong_nghe" => 0,
        "Xa_hoi" => 0,
        "Thoi_trang_Lam_dep" => 0,
        "Van_hoa_Giai_tri" => 0,
        "Phap_luat" => 0
      }
      
      (0..19).each do |i|
        path = "#{Rails.root}/app/assets/sample_data/#{i*1000}-#{(i+1)*1000}.txt.category"
        xml_str = "<SampleData>#{IO.read(path)}</SampleData>"
        xml = Nokogiri.XML(xml_str)
        arr = xml.xpath("//Document/Category")
        arr = arr.map {|a| a = a.text}

        puts "Processed #{i*1000}-#{(i+1)*1000}.txt.category - #{arr.count} articles"
        articles_count += arr.count

        categories_count.each do |cat, count|
          categories_count[cat] += arr.count cat
        end
      end

      categories_count
    end

    def replace_categories
      (0..19).each do |i|
        path = "#{Rails.root}/app/assets/#{i*1000}-#{(i+1)*1000}.txt.category"
        puts "Processing #{i*1000}-#{(i+1)*1000}.txt.category"

        out_file = File.open("#{path}_out", 'w')

        File.open(path).each do |line|
          CATEGORIES_MAP.each do |k, v|
            if line[/<Category>#{k}<\/Category>/]
              line = line.gsub(/<Category>#{k}<\/Category>/, "<Category>#{v}</Category>")
            end
          end

          out_file << line
        end

        out_file.close
      end
    end
  end
end