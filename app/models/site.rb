class Site
  include Mongoid::Document
  include Mongoid::CachedJson

  field :update_time, type: DateTime, default: ->{ DateTime.now }
  field :url
  field :title
  field :logo

  scope :domain, ->(domain) { return self.where(url: "http://#{domain}") }
  
  has_many :articles

  validates_presence_of :url, :title
  validates_uniqueness_of :update_time

  json_fields \
    _id: { },
    update_time: { definition: :update_time_rfc },
    title: { },
    logo: { }

  def self.crawl(domain)
    Crawler.new.crawl(domain)
  end

  def add_article(attributes)
    article = Article.find_or_initialize_by(url: attributes[:url])
    begin
      article.update_attributes!(attributes)
      self.articles << article
      self.save!
    rescue Mongoid::Errors::Validations => e
      false
    end
  end

  def update_time_rfc
    self.update_time.rfc822
  end

  def related_articles(page = 1, per = nil)
    results = articles.sort_by(&:posted_time).reverse
    results = Kaminari.paginate_array(articles).page(page).per(per)
  end

  private :update_time_rfc
end
