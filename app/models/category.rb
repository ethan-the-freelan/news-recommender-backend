# encoding: utf-8

class Category
  include Mongoid::Document
  include Mongoid::CachedJson

  CATEGORY_DATA = [
    {
      name: "Chính trị",
      tokenized_name: "Chinh_tri",
      sub_categories: ["Trong nước", "Thế giới", "Bình luận", "Bầu cử", "Quân sự"],
      tokenized_sub_categories: ["Trong_nuoc", "The_gioi", "Binh_luan", "Bau_cu", "Quan_su"]
    },
    {
      name: "Kinh tế - Tài chính",
      tokenized_name: "Kinh_te_Tai_chinh",
      sub_categories: ["Ngân hàng", "Đầu tư", "Bất động sản", "Chứng khoán", "Doanh nhân - Doanh nghiệp", "Mua sắm"],
      tokenized_sub_categories: ["Ngan_hang", "Dau_tu", "Bat_dong_san", "Chung_khoan", "Doanh_nhan_Doanh_nghiep", "Mua_sam"]
    },
    {
      name: "Đời sống",
      tokenized_name: "Doi_song",
      sub_categories: ["Sức khỏe", "Tình yêu", "Giới tính", "Hôn nhân", "Gia đình", "Ẩm thực", "Lối sống", "Phong cách"],
      tokenized_sub_categories: ["Suc_khoe", "Tinh_yeu", "Gioi_tinh", "Hon_nhan", "Gia_dinh", "Am_thuc", "Loi_song", "Phong_cach"]
    },
    {
      name: "Thể thao",
      tokenized_name: "The_thao",
      sub_categories: ["Việt Nam", "Quốc tế", "Bóng đá", "Quần vợt", "Đua xe"],
      tokenized_sub_categories: ["Viet_Nam", "Quoc_te", "Bong_da", "Quan_vot", "Dua_xe"]
    },
    {
      name: "Công nghệ",
      tokenized_name: "Cong_nghe",
      sub_categories: ["Xe cộ", "Điện thoại", "Máy tính", "Xu hướng"],
      tokenized_sub_categories: ["Xe_co", "Dien_thoai", "May_tinh", "Xu_huong"]
    },
    {
      name: "Xã hội",
      tokenized_name: "Xa_hoi",
      sub_categories: ["Thời sự", "Giáo dục", "Tuyển sinh", "Du học", "Việc làm"],
      tokenized_sub_categories: ["Thoi_su", "Giao_duc", "Tuyen_sinh", "Du_hoc", "Viec_lam"]
    },
    {
      name: "Thời trang - Làm đẹp",
      tokenized_name: "Thoi_trang_Lam_dep",
      sub_categories: ["Quần áo", "Mỹ phẩm", "Shop", "Spa", "Dịch vụ làm đẹp", "Xu hướng"],
      tokenized_sub_categories: ["Quan_ao", "My_pham", "Shop", "Spa", "Dich_vu_lam_dep", "Xu_huong"]
    },
    {
      name: "Văn hóa - Giải trí",
      tokenized_name: "Van_hoa_Giai_tri",
      sub_categories: ["Điện ảnh", "Sân khấu", "Âm nhạc", "Hậu trường", "Sao", "Game", "Sách"],
      tokenized_sub_categories: ["Dien_anh", "San_khau", "Am_nhac", "Hau_truong", "Sao", "Game", "Sach"]
    },
    {
      name: "Pháp luật",
      tokenized_name: "Phap_luat",
      sub_categories: ["An toàn giao thông", "Hình sự", "Văn bản Pháp luật"],
      tokenized_sub_categories: ["An_toan_giao_thong", "Hinh_su", "Van_ban_Phap_luat"]
    }
  ]

  field :name
  field :tokenized_name
  field :sub_categories
  field :tokenized_sub_categories

  attr_accessible :name, :tokenized_name, :sub_categories, :tokenized_sub_categories
  
  has_many :articles

  scope :tokenized_name, ->(token) { return self.where(tokenized_name: token) }

  json_fields \
    _id: { },
    name: { }

  def self.map_categories(category_data = CATEGORY_DATA)
    category_data.each do |cat|
      category = Category.find_or_initialize_by(tokenized_name: cat[:tokenized_name])
      category.update_attributes(cat)
      category.save!
    end
  end
end
