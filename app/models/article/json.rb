module Article::Json
  extend ActiveSupport::Concern

  def short_json
    {
      url: url,
      title: title,
      summary: summary,
    }
  end

  def related_articles_json
    self.related_articles.map(&:short_json)
  end

end