class Extractor
  PATH_EXTRACT = /^.*\//
  def extract(url)
    @extractor = CommonExtractors.ARTICLE_EXTRACTOR
    @highlighter = HTMLHighlighter.newExtractingInstance
    j_url = URL.new(url)
    extracted_content = @highlighter.process(j_url, @extractor)
    Sanitize.clean(extracted_content)
  end
end