# encoding: utf-8

require 'open-uri'

class Crawler
  include Crawler::SiteRules
  include Crawler::SiteUtils
  include Crawler::ArticleRules
  include Crawler::ArticleUtils

  def self.crawl_all_domain(reversed = false)
    start_time = Time.now
    crawler = Crawler.new

    if reversed
      SITE_RULES.reverse_each do |domain, properties|
        crawler.crawl_domain(domain)
      end
    else
      SITE_RULES.each do |domain, properties|
        crawler.crawl_domain(domain)
      end
    end

    "crawled #{Site.all.count} sites and #{Article.all.count} articles in time: #{Time.now - start_time}"
  end

  def self.crawl_all_article(reversed = false)
    crawler = Crawler.new

    if reversed
      SITE_RULES.reverse_each do |domain, properties|
        crawler.crawl_articles(domain)
      end
    else
      SITE_RULES.each do |domain, properties|
        crawler.crawl_articles(domain)
      end
    end

    "crawled #{Site.all.count} sites and #{Article.all.count} articles"
  end

  def ensure_url(url, domain)
    url_pattern = /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$/ix
    return url unless url =~ /^\// || url =~ url_pattern

    url.gsub!(/(\?|&|#)[\w\d]+(\=[\w\d]+)*/, '')       # Remove additional GET params from url
    return url if url =~ url_pattern

    return "http://#{domain}" + url
  end

  def self.crawl_header
    # agent = Mechanize.new
    # page = agent.get("http://tuoitre.vn/Chinh-tri-Xa-hoi/Index.html")
    # page = Nokogiri.HTML(page.content)
    # content = page.css("div[style='width: 672px; clear: both; overflow: hidden; margin-top: 5px; margin-bottom: 5px;']")

    # categories = []
    # domain = 'vnexpress.net'
    # a.each do |url|
    #   page = Nokogiri.HTML(open("http://#{domain}#{url}"))
    #   sub_cat = page.css("ul.ulSub > li.liSecond > h3 > a").map { |m| m[:href] }
    #   puts "#{sub_cat}"
    #   categories << sub_cat
    # end
    # categories.flatten!
    # return categories.count
  end
end