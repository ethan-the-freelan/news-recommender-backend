class ArticleClassifier
  include ArticleClassifier::FileUtil

  def initialize
    stop_words_file = "#{Rails.root}/lib/VnTextClassification/stopwords.txt"
    @stop_words = []
    File.open(stop_words_file, "r").each_line do |line|
      @stop_words << line.split(' ').first
    end

    @classifier = Classification.new "#{Rails.root}/lib/VnTextClassification/models"
    @classifier.init

    return true
  end

  def classify_all
    Site.all.each do |site|
      classify_site site
    end

    Article.all.select {|a| a.weight.nan?}.map {|a| a.delete}
  end

  def classify_site(site)
    site.articles.select {|a| a.category.nil?}.each do |article|
      result = classify_article article
      category = Category.where(tokenized_name: result[:category]).first
      article.category = category if category
      article.weight = result[:weight]
      article.save!
      puts "Classified article '#{article.title}' to category #{category.name} with weight #{article.weight}"
    end
  end

  def classify_article(article)
    return nil if article.extracted_content.nil? || article.extracted_content.blank?

    content = article.extracted_content
    tokenized_content = tokenize_content(content)
    category = @classifier.classify(tokenized_content)
    distribution = @classifier.getDistribution(tokenized_content)

    distArr = []

    for i in 0..distribution.size-1
      distArr << distribution.get(i).second.doubleValue
    end

    if (distArr.min < 0)
      min = distArr.min.abs
      distArr.map! {|d| d += min}
    end

    weight = distArr.max / distArr.sum

    {
      category: category,
      weight: weight
    }
  end

  def tokenize_content(content)
    tokens = content.split(' ')
    tokenized_arr = tokens

    for i in 0..tokens.count-2 do
      tokenized_arr << tokens[i] + "_" + tokens[i+1]
    end

    tokenized_arr = tokenized_arr - @stop_words
    tokenized_str = tokenized_arr.join(' ')
  end
end