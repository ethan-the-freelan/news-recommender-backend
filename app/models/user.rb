class User
  include Mongoid::Document
  include Mongoid::CachedJson
  include User::Rankings

  field :udid

  has_one :setting
  has_and_belongs_to_many :recommended_articles, class_name: "Article", inverse_of: nil
  has_and_belongs_to_many :browsed_articles, class_name: "Article", inverse_of: :browsing_users

  def self.find_or_create_by(attrs = {}, &block)
    user = super
    if user.category_rankings.nil?
      user.category_rankings = Hash[Category.all.map {|c| [c.tokenized_name, 0.to_f]}]
      user.sample_articles
      user.save
    end
    user
  end

  def browse_article(article)
    if browsed_articles.include? article
      false
    else
      browsed_articles << article
      category = article.category.tokenized_name
      category_rankings[category] += article.weight
      recommend_articles 30
      save
    end
  end

  def sample_articles(amount = 3)
    Category.all.each do |category|
      category.articles.sample(amount).each {|a| recommended_articles << a}
    end

    recommended_articles.shuffle!
  end
end
