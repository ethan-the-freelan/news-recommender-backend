module Crawler::SiteUtils
  extend ActiveSupport::Concern

  def crawl_domain(domain)
    start_time = Time.now
    @domain = domain
    @properties = Crawler::SiteRules::SITE_RULES[@domain]
    url = "http://#{domain}"
    page = Nokogiri::HTML(open(url))
    
    # Init site
    @site = Site.find_or_initialize_by(url: url)
    attributes = {
      update_time: DateTime.now,
      title: @properties[:title]
    }
    @site.update_attributes!(attributes)
    puts "==========\nCrawling site: #{domain}"
    
    # Categories
    @rules = @properties[:rules]
    
    @properties[:urls].each do |url|
      crawl_domain_url(url)
    end

    puts "\nCrawled #{@site.articles.select {|a| a.category.nil?}.count} articles from #{@site.title} in time: #{Time.now - start_time}"
    return
  end

  def crawl_domain_url(url)
    page = Nokogiri::HTML(open("http://#{@domain}#{url}"))
    puts "\nCrawling url #{url} from site #{@domain}"
    @properties[:selectors].each do |selector|
      news = page.css(selector)
      news.each do |content|
        attributes = {}
        @rules.each do |field, rule|
          rule.each do |exception|
            html = content.css(exception[:elm]).first
            next unless html

            if exception[:att]
              html = html[exception[:att]]
            else
              html = html.text[/[^>]*/]
            end

            if html
              html = html.gsub(/[\n\r]+/, ' ').gsub(/(^\s*|\s*$)/, '')
              attributes[field] = ensure_url(html, @domain)
              break
            end
          end
        end

        if @properties[:posted_time]
          @properties[:posted_time][:selectors].each do |exception|
            html = content.css(exception[:elm]).first
            next unless html

            if html
              begin
                if @properties[:posted_time][:format]
                  time = Time.strptime(html.text, @properties[:posted_time][:format])
                else
                  time = Time.parse html.text
                end
              rescue ArgumentError
              end

              if time
                attributes[:posted_time] = time
                break
              end
            end
          end
        end

        # puts "#{attributes}"
        puts "Crawled article: #{attributes[:title]}" if @site.add_article(attributes)
      end
    end
  end
end