# encoding: utf-8

module Crawler::ArticleUtils
  extend ActiveSupport::Concern
  include Crawler::ArticleRules

  def crawl_articles(domain)
    Site.domain(domain).first.articles.select {|a| a.extracted_content.nil? }.each do |article|
      if crawl_article_content(article)
        puts "Crawled article #{article.title} from #{article.site.title}"
      else
        puts "Invalid article #{article.url} - deleting"
        article.delete
      end
    end
  end

  def crawl_article_content(article)
    uri = URI.parse URI.encode(article.url)
    rule = Crawler::ArticleRules::ARTICLE_CONTENT_RULES[uri.host]

    return unless rule

    attributes = {}

    begin
      page = Nokogiri.HTML open(uri.to_s)
    rescue OpenURI::HTTPError
      return false
    end

    time = page.css(rule[:posted_time])
    unless time.nil? || time.first.nil? || time.text.blank?
      time = Time.parse time.text
      attributes[:posted_time] = time
    end

    content = page.css(rule[:content])

    unless content.nil? || content.first.nil?
      content = content.first.to_html
      content = Sanitize.clean(content, remove_contents: ['script', 'style'])
      tokens = /[\n\r\t\d\/\"\.\*\$\^\?\-',;:!~%#()=.*><“”…–]+/
      content.gsub!(tokens, ' ').gsub!(/\s+/, ' ')
      content = content.downcase
      attributes[:extracted_content] = content
    end

    if attributes[:extracted_content].nil? || attributes[:extracted_content].blank?
      return false
    else
      return article.update_attributes!(attributes)
    end
  end
end