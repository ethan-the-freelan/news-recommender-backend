module Crawler::ArticleRules
  extend ActiveSupport::Concern

  ARTICLE_CONTENT_RULES = {
    'hn.24h.com.vn' => {
      posted_time: 'div.publication_date',
      content: 'div.text-conent'
    },
    'afamily.vn' => {
      posted_time: 'div.info_other.fl.mgt10 > fl',
      content: 'div.detail_content.fl.mgt15'
    },
    'bongdaplus.vn' => {
      posted_time: 'div.date',
      content: 'div.story-body'
    },
    'dantri.com.vn' => {
      posted_time: 'span.fr.fon7.mr2',
      content: 'div.fon34.mt3.mr2.fon43'
    },
    'kenh14.vn' => {
      posted_time: 'div.meta',
      content: 'div.content'
    },
    'laodong.com.vn' => {
      posted_time: 'div.tool_bar',
      content: 'div.article_content'
    },
    'news.zing.vn' => {
      posted_time: 'span.timestamp',
      content: '#content_document'
    },
    'ngoisao.net' => {
      posted_time: '#pDateTime',
      content: 'div.detailCT'
    },
    'nhandan.org.vn' => {
      posted_time: 'div.articleupdated',
      content: 'div.parentfolder-content'
    },
    'tiin.vn' => {
      posted_time: 'p#time',
      content: 'div#body-content'
    },
    'tuoitre.vn' => {
      posted_time: "span[style='line-height: 27px; padding-left: 0px;'] script",
      content: '#divContent'
    },
    'vietnamnet.vn' => {
      posted_time: 'ul.buttonFunctionBox > li',
      content: 'div.articleDetailBox'
    },
    'vnexpress.net' => {
      posted_time: 'span.spanTime',
      content: 'div.content'
    },
  }
end