module User::Rankings
  extend ActiveSupport::Concern

  def sorted_recommendations
    tokenized_arr = recommended_articles.map {|a| a.category.tokenized_name}
    recommended_articles.sort do |a, b|
      tokenized_arr.count(b.category.tokenized_name) <=> tokenized_arr.count(a.category.tokenized_name)
    end
  end

  def recommend_articles(articles_count = 20)
    recommended_articles.clear

    # browsed categories have positive random distributions
    browsed_cats = rank_categories articles_count

    browsed_cats.each do |tokenized_name, prob|
      category = Category.tokenized_name(tokenized_name).first
      browseds = browsed_articles.select {|a| a.category.tokenized_name.eql?(tokenized_name)}
      (category.articles - browseds).reverse.first(prob).each {|a| recommended_articles << a}
    end

    # non-browsed categories have zero random distributions or zero ranking probability
    other_cats = Category.all.map {|c| c.tokenized_name} - recommended_articles.map {|a| a.category.tokenized_name}.uniq

    other_cats.each do |tokenized_name|
      category = Category.tokenized_name(tokenized_name).first
      recommended_articles << category.articles.last
    end

    recommended_articles.map {|a| a.category.tokenized_name}
  end

  def rank_categories(articles_count = 20)
    rankings = calculate_rankings
    category_rankings = {}
    rankings.each {|k, v| category_rankings[k] = 0}

    for i in 1..articles_count+20
      category = random_distribute rankings

      next if i <= 20

      category_rankings[category] += 1
    end

    Hash[category_rankings.sort {|a, b| b[1] <=> a[1]}]
  end

  def calculate_rankings
    # init rankings hash with all 0.0 ranking value
    rankings = Hash[Category.all.map {|c| [c.tokenized_name, 0.to_f]}]

    browsed_articles.map do |a|
      rankings[a.category.tokenized_name] += category_rankings[a.category.tokenized_name]*a.weight
    end

    sum = rankings.values.sum
    rankings.each do |k, v|
      rankings[k] = v/sum
      rankings.delete k if v == 0.to_f
    end
    rankings
  end

  def random_distribute(rankings)
    random = Random.rand
    for i in 0..rankings.count-2
      return rankings.keys[i] if random.between?(rankings.values.first(i).sum, rankings.values.first(i+1).sum)
    end

    rankings.keys.last
  end
end