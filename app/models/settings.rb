class Settings
  include Mongoid::Document
  field :udid, type: String
  field :fav_sites, type: Array
  field :fav_categories, type: Array

  belongs_to :user
end
