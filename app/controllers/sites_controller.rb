class SitesController < ApplicationController
  before_filter :check_udid

  def check_udid
    unless params[:udid]
      render json: { status: 1, error: "No UDID" }
    end
  end

  def index
    unless params[:domain]
      render json: { status: 1, message: 'Invalid request' }
      return
    end

    site = Site.domain(params[:domain]).first

    unless site
      render json: { status: 2, message: 'Domain not found' }
      return
    end
    
    articles = site.related_articles(params[:page], params[:per_page])
    render json: {
      status: 0,
      site: site,
      total_articles: site.articles.count,
      results: articles
    }
  end

  def all_info
    render json: {
      status: 0,
      sites: Site.all.sort_by {|s| s.title},
      categories: Category.all.sort_by { |c| c.name }
    }
  end

  def all_sites
    render json: { status: 0, sites: Site.all.sort_by {|s| s.title} }
  end

  def all_categories
    render json: { status: 0, categories: Category.all }
  end
end
