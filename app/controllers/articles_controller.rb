class ArticlesController < ApplicationController
  before_filter :check_udid

  def check_udid
    unless params[:udid]
      render json: { status: 1, error: "UDID required" }
    end
  end

  def all
    articles = Article.all.sort_by(&:posted_time)
    xml = articles.to_xml(skip_types: true)

    send_data xml, filename: "articles.xml"
  end

  def by_site
    begin
      site = Site.find(params[:site_id])
    rescue Mongoid::Errors::DocumentNotFound => e
      render json: { status: 1, error: "Site not found" }
    end

    if site
      articles = site.articles.desc(:posted_time)
      articles = articles.page(params[:page]).per(params[:per_page])
      render json: { status: 0, results: articles }
    end
  end

  def by_category
    begin
      category = Category.find(params[:category_id])
    rescue Mongoid::Errors::DocumentNotFound => e
      render json: { status: 1, error: "Category not found" }
    end

    if category
      articles = category.articles.desc(:posted_time)
      articles = articles.page(params[:page]).per(params[:per_page])
      render json: { status: 0, results: articles }
    end
  end
end
