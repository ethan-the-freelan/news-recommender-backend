class ClassifiersController < ApplicationController
  before_filter :check_initialize, except: :index

  def check_initialize
    unless defined?(@@articles) && defined?(@@all_categories) && defined?(@@classifier) && !@@articles.empty?
      redirect_to classifiers_path
    end
  end

  def index
    @@articles = Article.where(category_id: nil).sample 1000
    @@all_categories = Category.all
    @@classifier = ArticleClassifier.new
    @@auto = 0
    @@manual = 0
  end

  def classify
    @article = @@articles.pop
    @categories = @@all_categories
    classified = @@classifier.classify_article @article
    @suggestion = classified[:category] unless classified.nil?
  end

  def apply
    if params[:article_id].nil? && params[:category_id].nil?
      @@auto += 1
    else
      article = Article.find(params[:article_id])
      category = Category.find(params[:category_id])

      category.articles << article
      category.save!

      @@manual += 1
    end

    puts "Rate ~~~~> #{@@manual} - #{@@auto}"
    
    redirect_to classify_classifiers_path
  end

  def delete_article
    unless params[:article_id].nil?
      article = Article.find(params[:article_id])
      article.delete
    end

    redirect_to classify_classifiers_path
  end
end
