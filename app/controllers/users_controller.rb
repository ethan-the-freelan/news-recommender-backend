class UsersController < ApplicationController
  before_filter :check_udid
  before_filter :check_article_id, except: [:browsed_articles, :recommended_articles]

  def check_udid
    unless params[:udid]
      render json: { status: 1, error: "UDID required" }
    end
  end

  def check_article_id
    unless params[:article_id]
      render json: { status: 1, error: "Article ID required" }
    end
  end

  def browsed_articles
    user = User.where(udid: params[:udid]).first
    
    if user.nil?
      render json: { status: 1, error: "User not found" }
    else
      render json: { status: 0, results: { articles_count: user.browsed_articles.count, articles: user.browsed_articles } }
    end
  end

  def browse_article
    begin
      user = User.find_or_create_by(udid: params[:udid])
      article = Article.find(params[:article_id])
    rescue Mongoid::Errors::DocumentNotFound => e
      render json: { status: 1, error: "Article not found" }
    end

    if user && article
      if user.browse_article(article)
        render json: { status: 0, result: "User has successfully browsed article" }
      else
        render json: { status: 1, result: "User has already browsed article"}
      end
    end
  end

  def recommended_articles
    user = User.find_or_create_by(udid: params[:udid])
    render json: { status: 0, results: user.sorted_recommendations }
  end
end
